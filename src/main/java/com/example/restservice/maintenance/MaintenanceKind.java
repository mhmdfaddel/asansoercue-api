package com.example.restservice.maintenance;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class MaintenanceKind {

	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long maintenkind_id;

	private String name;

	
	public Long getMaintenkind_id() {
		return maintenkind_id;
	}

	public void setMaintenkind_id(Long maintenkind_id) {
		this.maintenkind_id = maintenkind_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}

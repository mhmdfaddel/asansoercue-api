package com.example.restservice.maintenance;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MaintenanceController {
	private final MaintenanceRepo repository;

    MaintenanceController(MaintenanceRepo repository) {
        this.repository = repository;
    }

    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/maintenances")
    List<Maintenance> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    @PostMapping("/maintenances")
    Maintenance newMaintenance(@RequestBody Maintenance newMaintenance) {
        return repository.save(newMaintenance);
    }

    // Single item

    @GetMapping("/maintenances/{id}")
    Maintenance one(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new MaintenanceNotFoundException(id));
    }

    @PutMapping("/maintenances/{id}")
    Maintenance replaceMaintenance(@RequestBody Maintenance newMaintenance, @PathVariable Long id) {
//
//        return repository.findById(id).map(maintenance -> {
//            maintenance.setName(newMaintenance.getName());
//            maintenance.setRole(newMaintenance.getRole());
//            return repository.save(maintenance);
//        }).orElseGet(() -> {
//            newMaintenance.setMaintenance_id(id);
            return repository.save(newMaintenance);
//        });
    }

    @DeleteMapping("/maintenances/{id}")
    void deleteMaintenance(@PathVariable Long id) {
        repository.deleteById(id);
    }
}

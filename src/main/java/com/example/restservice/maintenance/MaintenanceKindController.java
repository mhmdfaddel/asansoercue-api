package com.example.restservice.maintenance;


import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class MaintenanceKindController {
	private final MaintenanceKindRepo repository;

    MaintenanceKindController(MaintenanceKindRepo repository) {
        this.repository = repository;
    }

    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/maintenanceKinds")
    List<MaintenanceKind> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    @PostMapping("/maintenanceKinds")
    MaintenanceKind newMaintenanceKind(@RequestBody MaintenanceKind newMaintenanceKind) {
        return repository.save(newMaintenanceKind);
    }

    // Single item

    @GetMapping("/maintenanceKinds/{id}")
    MaintenanceKind one(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new MaintenanceKindNotFoundException(id));
    }

    @PutMapping("/maintenanceKinds/{id}")
   MaintenanceKind replaceMaintenanceKind(@RequestBody MaintenanceKind newMaintenanceKind, @PathVariable Long id) {

//        return repository.findById(id).map(maintenanceKind -> {
//            maintenanceKind.setName(newMaintenanceKind.getName());
//            maintenanceKind.setRole(newMaintenanceKind.getRole());
//            return repository.save(maintenanceKind);
//        }).orElseGet(() -> {
//            newMaintenanceKind.setMaintenanceKind_id(id);
            return repository.save(newMaintenanceKind);
//        });
    }

    @DeleteMapping("/maintenanceKinds/{id}")
    void deleteMaintenanceKind(@PathVariable Long id) {
        repository.deleteById(id);
    }
}

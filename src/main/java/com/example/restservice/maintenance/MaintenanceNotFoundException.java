package com.example.restservice.maintenance;

class MaintenanceNotFoundException extends RuntimeException {

	MaintenanceNotFoundException(Long id) {
		super("Could not find Maintenance " + id);
	}
}
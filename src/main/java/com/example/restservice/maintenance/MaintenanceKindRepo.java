package com.example.restservice.maintenance;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenanceKindRepo extends JpaRepository<MaintenanceKind, Long>{

}

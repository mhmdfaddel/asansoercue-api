package com.example.restservice.maintenance;

class MaintenanceKindNotFoundException extends RuntimeException {

	MaintenanceKindNotFoundException(Long id) {
		super("Could not find Maintenance Kind " + id);
	}
}
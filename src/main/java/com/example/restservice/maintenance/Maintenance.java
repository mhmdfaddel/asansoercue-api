package com.example.restservice.maintenance;

import com.example.restservice.employees.Employee;
import com.example.restservice.lift.Lift;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.sql.Date;
import java.util.*;


@Entity
public class Maintenance {

	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long maintenance_id;
	
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
	@JoinTable(
            name = "Employee_Maintenance",
            joinColumns = {@JoinColumn(name = "employee_id")},
            inverseJoinColumns = {@JoinColumn(name = "maintenance_id")}
    )
	private Set<Employee> ChargedEmployees = new HashSet<>();

	private Date date;

	private String description;

	//private MaintenKind typo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "lift_Maintenance_id", nullable = false)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)

	private Lift lift;
	
	public Long getMaintenance_id() {
		return maintenance_id;
	}

	public void setMaintenance_id(Long maintenance_id) {
		this.maintenance_id = maintenance_id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public Lift getLift() {
		return lift;
	}

	public void setLift(Lift lift) {
		this.lift = lift;
	}

	public Set<Employee> getChargedEmployees() {
		return ChargedEmployees;
	}

	public void setChargedEmployees(Set<Employee> chargedEmployees) {
		ChargedEmployees = chargedEmployees;
	}
}










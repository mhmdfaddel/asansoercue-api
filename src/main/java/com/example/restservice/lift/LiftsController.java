package com.example.restservice.lift;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
public class LiftsController {
	private final LiftRepo repository;

    LiftsController(LiftRepo repository) {
        this.repository = repository;
    }

    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/lifts")
    List<Lift> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]

    @PostMapping("/lifts")
    Lift newLift(@RequestBody Lift newLift) {
        return repository.save(newLift);
    }

    // Single item

    @GetMapping("/lifts/{id}")
    Lift one(@PathVariable Long id) {

        return repository.findById(id).orElseThrow(() -> new LiftNotFoundException(id));
    }

    @PutMapping("/lifts/{id}")
    Lift replaceLift(@RequestBody Lift newLift, @PathVariable Long id) {

//        return repository.findById(id).map(lifts -> {
//            lifts.setName(newLift.getName());
//            lifts.setRole(newLift.getRole());
//            return repository.save(lifts);
//        }).orElseGet(() -> {
//            newLift.setLift_id(id);
            return repository.save(newLift);
//        });
    }

    @DeleteMapping("/lifts/{id}")
    void deleteLift(@PathVariable Long id) {
        repository.deleteById(id);
    }
}

package com.example.restservice.lift;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LiftRepo extends JpaRepository<Lift, Long>{

}

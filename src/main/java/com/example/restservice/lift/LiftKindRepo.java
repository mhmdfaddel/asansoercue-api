package com.example.restservice.lift;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LiftKindRepo extends JpaRepository<LiftKind, Long>{

}

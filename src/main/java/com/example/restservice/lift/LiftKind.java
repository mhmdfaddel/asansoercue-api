package com.example.restservice.lift;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class LiftKind {
	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long liftkind_id;

	private String name;

	public Long getLiftkind_id() {
		return liftkind_id;
	}

	public void setLiftkind_id(Long liftkind_id) {
		this.liftkind_id = liftkind_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

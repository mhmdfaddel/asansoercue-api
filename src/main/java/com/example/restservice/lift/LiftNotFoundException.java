package com.example.restservice.lift;

class LiftNotFoundException extends RuntimeException {

	LiftNotFoundException(Long id) {
		super("Could not find Lift " + id);
	}
}
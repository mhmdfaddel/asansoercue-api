package com.example.restservice.lift;



import com.example.restservice.maintenance.Maintenance;

import javax.persistence.*;
import java.util.List;

@Entity

public class Lift {
	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long lift_id;
	
	private String kimlikNo, phoneNo, buildingNo, buildingname, installerName, installerPhone, status;
	
	private int buildingApts;
	
	private double longitude;

	private double latitude;
	
	@OneToMany(mappedBy = "lift", cascade = CascadeType.ALL)
	private List<Maintenance> maintenances;

	

	public Long getLift_id() {
		return lift_id;
	}

	public void setLift_id(Long lift_id) {
		this.lift_id = lift_id;
	}

	public String getKimlikNo() {
		return kimlikNo;
	}

	public void setKimlikNo(String kimlikNo) {
		this.kimlikNo = kimlikNo;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getBuildingNo() {
		return buildingNo;
	}

	public void setBuildingNo(String buildingNo) {
		this.buildingNo = buildingNo;
	}

	public String getBuildingname() {
		return buildingname;
	}

	public void setBuildingname(String buildingname) {
		this.buildingname = buildingname;
	}

	public String getInstallerName() {
		return installerName;
	}

	public void setInstallerName(String installerName) {
		this.installerName = installerName;
	}

	public String getInstallerPhone() {
		return installerPhone;
	}

	public void setInstallerPhone(String installerPhone) {
		this.installerPhone = installerPhone;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<Maintenance> getMaintenances() {
		return maintenances;
	}

	public void setMaintenances(List<Maintenance> maintenances) {
		this.maintenances = maintenances;
	}

}

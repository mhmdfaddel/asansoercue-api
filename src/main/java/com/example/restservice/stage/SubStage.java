package com.example.restservice.stage;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;


@Entity
public class SubStage {

	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
	
	private String name;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "stage_subStage_id", nullable = false)
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	private Stage stage;


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Stage getStage() {
		return stage;
	}


	public void setStage(Stage stage) {
		this.stage = stage;
	}
	
	
	
}

package com.example.restservice.stage;

import javax.persistence.*;
import java.util.List;

@Entity
public class Stage {

	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;

	private String name;
	
	@OneToMany(mappedBy = "stage", cascade = CascadeType.ALL)
	private List<SubStage> substages;
	
	private boolean checked;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<SubStage> getSubstages() {
		return substages;
	}

	public void setSubstages(List<SubStage> substages) {
		this.substages = substages;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}
	
	
}

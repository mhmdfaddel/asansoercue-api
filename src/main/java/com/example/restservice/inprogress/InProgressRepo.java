package com.example.restservice.inprogress;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InProgressRepo extends JpaRepository<InProgress, Long>{

}

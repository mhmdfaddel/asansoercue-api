package com.example.restservice.employees;

import com.example.restservice.maintenance.Maintenance;

import javax.persistence.*;
import java.util.Objects;
import java.util.*;

@Entity
public class Employee {

	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long employee_id;
	private String name;
	private String role;
	
	@ManyToMany(mappedBy = "ChargedEmployees", cascade = CascadeType.ALL)
	private Set<Maintenance> maintenances= new HashSet<>();

	public Employee() {

	}


	public Long getEmployee_id() {
		return employee_id;
	}

	public void setEmployee_id(Long employee_id) {
		this.employee_id = employee_id;
	}

	public String getName() {
		return this.name;
	}

	public String getRole() {
		return this.role;
	}


	public void setName(String name) {
		this.name = name;
	}

	public void setRole(String role) {
		this.role = role;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Employee))
			return false;
		Employee employee = (Employee) o;
		return Objects.equals(this.employee_id, employee.employee_id) && Objects.equals(this.name, employee.name)
				&& Objects.equals(this.role, employee.role);
	}

	@Override
	public int hashCode() {
		return Objects.hash(this.employee_id, this.name, this.role);
	}

	public Set<Maintenance> getMaintenances() {
		return maintenances;
	}

	public void setMaintenances(Set<Maintenance> maintenances) {
		this.maintenances = maintenances;
	}

}